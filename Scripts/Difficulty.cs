﻿using System.Collections;
using System;
/// <summary>
/// Difficulty class sets the difficulty through out the game.
/// </summary>
public class Difficulty {

	//   Always start on impossible if not set.
	private int difficulty;

	/// <summary>
	/// Initializes a new instance of the <see cref="Difficulty"/> class.
	/// </summary>
	/// <param name="level">Level.</param>
	public Difficulty (int level) {
		if (level <= 25) difficulty = 25;
		else if (level <= 50 && level > 25) difficulty = 50;
		else if (level <= 75 && level > 50) difficulty = 75;
		else if (level >= 100 && level > 75) difficulty = 100;
	}

	public Difficulty () {
	} //   Empty constructor

	/// <summary>
	/// Checks to see if the move can be fired.
	/// </summary>
	/// <returns><c>true</c>, if successful, fire the rule, <c>false</c> otherwise.</returns>
	public bool isSuccessful() {
		Random random = new Random();
		int rNumber = random.Next (0,100); //  101 is not included.
										   //  Actual value between 0 -100

		return rNumber <= difficulty ? true : false;
	}

	/// <summary>
	/// Sets the difficulty.
	/// </summary>
	/// <param name="difficulty">Difficulty.</param>
	public void setDifficulty (int difficulty) {
		this.difficulty = difficulty;
	}

	/// <summary>
	/// Sets to easy.
	/// </summary>
	/// <param name="easy">If set to <c>true</c> easy.</param>
	public void setToEasy(bool easy) {
		if (easy) difficulty = 25; //   % chance
	}

	/// <summary>
	/// Sets to medium.
	/// </summary>
	/// <param name="medium">If set to <c>true</c> medium.</param>
	public void setToMedium(bool medium) {
		if (medium) difficulty = 50; //   % chance
	}

	/// <summary>
	/// Sets to easy.
	/// </summary>
	/// <param name="hard">If set to <c>true</c> hard.</param>
	public void setToHard(bool hard) {
		if (hard) difficulty = 75; //   % chance
	}

	/// <summary>
	/// Sets to impossible.
	/// </summary>
	/// <param name="impossible">If set to <c>true</c> impossible.</param>
	public void setToImpossible(bool impossible) {
		if (impossible) difficulty = 100; //   % chance
	}

	/// <summary>
	/// Gets the difficulty.
	/// </summary>
	public int getDifficulty () {
		return difficulty;
	}

	public Node makeRandomMove() {
		Random rY = new Random();
		Random rX = new Random();

		int x = rY.Next(0, 3);
		int y = rX.Next(0, 3);

		Node node = new Node (x, y);

		return node;

	}
}
