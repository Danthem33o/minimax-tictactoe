﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Node class holds all of the move information.
/// 
/// It takes the coordinates of the move mad (X and Y) and the heuristic
/// score of the node to indicate a win, loss or draw; i.e. either 10, -10 or -1.
/// </summary>
public class Node  {

	//   Int variables
	//   x and y are coodinates
	int x, y, score;

	/// <summary>
	/// Initializes a new instance of the <see cref="Node"/> class
	/// that takes the x and y coordinates and the score of the node.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="score">Score of the node</param>
	public Node(int x, int y, int score) {
		this.x = x;
		this.y = y;
		this.score = score;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Node"/> class 
	/// that takes the x and y coordinates.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public Node(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Node"/> class
	/// that takes the score of the node.
	/// </summary>
	/// <param name="score">Score.</param>
	public Node(int score) {
		this.score = score;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Node"/> class.
	/// </summary>
	public Node() {
	} //   Empty Constructor

	/// <summary>
	/// Sets the x coordinate.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	public void setX(int x) {
		this.x = x;
	}

	/// <summary>
	/// Sets the X coordinate.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	public void setXX(int x) {
		this.x = x;
	}

	/// <summary>
	/// Gets the x coordinate of the node.
	/// </summary>
	/// <returns>The x coordinate.</returns>
	public int getX() {
		return x;
	}

	/// <summary>
	/// Sets the y coordinate of the node.
	/// </summary>
	/// <param name="y">The y coordinate.</param>
	public void setY(int y) {
		this.y = y;
	}

	/// <summary>
	/// Gets the y coordinate of the node.
	/// </summary>
	/// <returns>The y coordinate.</returns>
	public int getY() {
		return y;
	}

	/// <summary>
	/// Sets the score of the node.
	/// </summary>
	/// <param name="score">Score of the node.</param>
	public void setScore(int score) {
		this.score = score;
	}

	/// <summary>
	/// Gets the score of the node.
	/// </summary>
	/// <returns>The score of the node.</returns>
	public int getScore() {
		return score;
	}
}
