﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/// <summary>
/// Player move action listener class.
/// 
/// Waits for the user to make a move and then sets the tile that was
/// clicked on to green (indicates the user's turn).
/// </summary>
public class PlayerMove : MonoBehaviour {

	//   Vars
	public Game game;
	public Button thisButton;

	/// <summary>
	/// Action listener method that waits to se which button was clicked
	/// and then changes the colour of the square to the user's colour.
	/// </summary>
	public void buttonPressed() {
		bool playerturn;
		playerturn = game.playerTurn;

		//   Find the button clicked
		thisButton = GetComponent<Button> ();

		//	 Get the button clicked
		if (playerturn && thisButton.image.color == Color.gray) {
			thisButton.image.color = Color.green;

			//   Change the turn when done
			game.setPlayerTurn (false);
		} 


	}

} // End class
