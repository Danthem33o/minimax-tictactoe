﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/// <summary>
/// Game class.
/// 
/// Always starts on players turn to allow for a draw or a win given perfect play. 
/// The user's move is handled in the playerMove class and the AI in the aiMove() method.
/// 
/// Green: Player.
/// Grey: Neutral.
/// Red: AI.
/// </summary>
public class Game : MonoBehaviour {

	//	Boolean vars
	public bool playerTurn;
	public bool userWon;
	public bool aiWon;
	public bool draw;
	public bool gameOver;

	MiniMax minimax;

	//	array of buttons -i.e. the board
	public Button[,] board;

	//	buttons to be added in array
	public Button piece11;
	public Button piece12;
	public Button piece13;
	public Button piece21;
	public Button piece22;
	public Button piece23;
	public Button piece31;
	public Button piece32;
	public Button piece33;

	// Use this for initialization
	void Start () {

		//	Initialise boolean vars
		draw = false;
		aiWon = false;
		userWon = false;
		gameOver = false;
		playerTurn = true;

		//	Initialise buttons and add to array
		board = new Button[3,3];
		board[0,0] = piece11.GetComponent<Button>();
		board[0,1] = piece12.GetComponent<Button>();
		board[0,2] = piece13.GetComponent<Button>();
		board[1,0] = piece21.GetComponent<Button>();
		board[1,1] = piece22.GetComponent<Button>();
		board[1,2] = piece23.GetComponent<Button>();
		board[2,0] = piece31.GetComponent<Button>();
		board[2,1] = piece32.GetComponent<Button>();
		board[2,2] = piece33.GetComponent<Button>();

		//	Set the colour of the board to grey
		//	and reset any possible value
		resetBoard ();

	}
	
	// Update is called once per frame
	void Update () {

		//	Make AI move when the player is finished
		//	player turn is changed in the PlayerMove class
		// 	and aiMove() method.
		if (!playerTurn)
			aiMove ();

		//	Check for outcome after the move
		hasWon();
	}

	/// <summary>
	/// Takes the AI's turn. The move is determined by the minimax class.
	/// The game state is passed through to the minimax class to allow for alteration.
	/// 
	/// AI's move is made within the minimax class.
	/// </summary>
	public void aiMove() {
		Difficulty difficulty = new Difficulty (100);
		if (difficulty.isSuccessful ()) {
			minimax = new MiniMax (this); // Move is made in minimax
		} else {
			hasWon ();
			if (!gameOver) {
				Node randomMove = difficulty.makeRandomMove ();  //   Make a random move
				if (isSpaceAt (randomMove.getX (), randomMove.getY ()))
					makeMove (false, randomMove.getX (), randomMove.getY ());
				else
					aiMove ();
			} else playerTurn = true;

		}

		playerTurn = true;			 // Change the move back to player
	}

	/// <summary>
	/// Resets the board's values back to starting state, i.e. grey.
	/// </summary>
	public void resetBoard() {
		for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
				board [x, y].GetComponent<Button> ().image.color = Color.gray;
	}

	/// <summary>
	/// Makes either the AI or player move. The X and Y coordinates is the 
	/// location on the board.
	/// </summary>
	/// <param name="player">If set to <c>true</c> it is the players turn
	/// else it is the AI's turn.</param>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public void makeMove(bool player,int x, int y) {
		//	if space
		if (isSpaceAt (x, y)) {
			if (player)
				board [x, y].GetComponent<Button> ().image.color = Color.green;
			else
				board [x, y].GetComponent<Button> ().image.color = Color.red;
		} else {
			Console.Write ("No more moves");
		}
	}

	/// <summary>
	/// Undos the move specified at the location X and Y.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public void undoMove(int x, int y) {
		board [x, y].GetComponent<Button> ().image.color = Color.gray;
	}

	/// <summary>
	/// Checks to see if there is space anywhere on the board.
	/// </summary>
	/// <returns><c>true</c>, if there is space on the board <c>false</c> 
	/// otherwise.</returns>
	public bool isSpace() {
		for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
				if (!board [x, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
				    !board [x, y].GetComponent<Button> ().image.color.Equals (Color.red))
					return true;
				
		return false;
					
	}

	/// <summary>
	/// Checks for space at the location specified at X and Y.
	/// </summary>
	/// <returns><c>true</c>, if there is space, <c>false</c> otherwise.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public bool isSpaceAt(int x, int y) {
		return (!board [x, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
		!board [x, y].GetComponent<Button> ().image.color.Equals (Color.red));

	}

	/// <summary>
	/// Checks to see if a player has won or drew. 
	/// 
	/// It will either set the userWon, draw or aiWon boolean.
	/// </summary>
	public void hasWon() {
		//	Vertical
		//	User
		int y = 0;
		int x = 0;
		for (x = 0; x < 3; x++)
			if (board [x, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
			     board [x, y + 1].GetComponent<Button> ().image.color.Equals (Color.green) &&
			     board [x, y + 2].GetComponent<Button> ().image.color.Equals (Color.green)) {

				userWon = true;
				gameOver = true;
				break;

			}

		//	AI
		y = 0;
		x = 0;
		for (x = 0; x < 3; x++)
			if (board [x, y].GetComponent<Button> ().image.color.Equals (Color.red) &&
				board [x, y + 1].GetComponent<Button> ().image.color.Equals (Color.red) &&
				board [x, y + 2].GetComponent<Button> ().image.color.Equals (Color.red)) {

				aiWon = true;
				gameOver = true;
				break;

			}

		//	Horizontal
		//	user
		x = 0;
		for (y = 0; y < 3; y++)
			if (board [x, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
				board [x + 1, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
				board [x + 2, y].GetComponent<Button> ().image.color.Equals (Color.green)) {
				userWon = true;
				gameOver = true;
				break;
			}

		//`	AI
		x = 0;
		for (y = 0; y < 3; y++)
			if (board [x, y].GetComponent<Button> ().image.color.Equals (Color.red) &&
				board [x + 1, y].GetComponent<Button> ().image.color.Equals (Color.red) &&
				board [x + 2, y].GetComponent<Button> ().image.color.Equals (Color.red)) {
				aiWon = true;
				gameOver = true;
				break;
			}
		

		//	Diagonal
		//	user
		x = 0; y = 0;
		if(board [x, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
			board [x + 1, y + 1].GetComponent<Button> ().image.color.Equals (Color.green) &&
			board [x + 2, y + 2].GetComponent<Button> ().image.color.Equals (Color.green)) 
		{
			userWon = true;
			gameOver = true;
		}

		x = 0; y = 0;
		if(board [x, y + 2].GetComponent<Button> ().image.color.Equals (Color.green) &&
			board [x + 1, y + 1].GetComponent<Button> ().image.color.Equals (Color.green) &&
			board [x + 2, y].GetComponent<Button> ().image.color.Equals (Color.green)) 
		{
			userWon = true;
			gameOver = true;
		}

		//	AI
		x = 0; y = 0;
		if(board [x, y].GetComponent<Button> ().image.color.Equals (Color.red) &&
			board [x + 1, y + 1].GetComponent<Button> ().image.color.Equals (Color.red) &&
			board [x + 2, y + 2].GetComponent<Button> ().image.color.Equals (Color.red)) 
		{
			aiWon = true;
			gameOver = true;
		}

		x = 0; y = 0;
		if(board [x, y + 2].GetComponent<Button> ().image.color.Equals (Color.red) &&
			board [x + 1, y + 1].GetComponent<Button> ().image.color.Equals (Color.red) &&
			board [x + 2, y].GetComponent<Button> ().image.color.Equals (Color.red)) 
		{
			aiWon = true;
			gameOver = true;
		}
			
		//	draw
		bool flag = true;
		for (x = 0; x < 3; x++) {
			for (y = 0; y < 3; y++) {
				if (!board [x, y].GetComponent<Button> ().image.color.Equals (Color.green) &&
					!board [x, y].GetComponent<Button> ().image.color.Equals (Color.red)) {
					flag = false;
					break;
				}	
			}
		}

		if (flag) {
			draw = true;
			gameOver = true;
		}
	}

	/// <summary>
	/// Gets the board.
	/// </summary>
	/// <returns>The board.</returns>
	public Button[,] getBoard() {
		return board;
	}

	/// <summary>
	/// Sets the player turn.
	/// </summary>
	/// <param name="playerTurn">If set to <c>true</c> player turn.</param>
	public void setPlayerTurn(bool playerTurn) {
		this.playerTurn = playerTurn;
	}

	/// <summary>
	/// Resets the game boolean variables.
	/// </summary>
	public void resetGameVars() {
		draw = false;
		aiWon = false;
		userWon = false;
		gameOver = false;
	}
}
